class IVTViolationException(Exception):
    def __init__(self, message, left_endpt, right_endpt):
        super().__init__(self, message)
        self.left_endpt = left_endpt
        self.right_endpt = right_endpt

    def endpts(self):
        return self.left_endpt, self.right_endpt


class DiscontinuityException(Exception):
    def __init__(self, message, *args):
        super().__init__(self, message)
        self.args = args


class ZeroSlopeException(Exception):
    def __init__(self, message, delta, *args):
        super().__init__(self, message)
        self.delta = delta
        self.args = args


class ExceededMaxIterException(Exception):
    def __init__(self, message, maxiter):
        super().__init__(self, message)
        self.maxiter = maxiter


class DuplicateXException(Exception):
    def __init__(self, message, *args):
        super().__init__(self, message)
        self.args = args


class InvalidPtListException(Exception):
    def __init__(self, message, pt_list):
        super().__init__(self, message)
        self.pt_list = pt_list
