from typing import Callable, Generator, Tuple

MathFunction = Callable[[float], float]
FloatGen = Generator[float, None, None]
