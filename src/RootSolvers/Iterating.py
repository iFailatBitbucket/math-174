
from abc import abstractmethod
from sys import float_info
from typing import Callable

from pandas import Series

from ..CustomTypes import MathFunction
from ..Exceptions import (DiscontinuityException, ExceededMaxIterException,
                          ZeroSlopeException)
from .RootSolver import RootSolver


class Iterating(RootSolver):

    _f_prev = None

    def __init__(self,
                 fxn: MathFunction,
                 x0: float,
                 epsilon: float = float_info.epsilon,
                 max_iter: int = 1000
                 ):
        super().__init__(fxn, epsilon)
        self._x0 = x0
        self._max_iter = max_iter

        self._x_curr = x0
        self._f_curr = fxn(x0)

    def solve_root(self, record_log: bool = False) -> float:
        if record_log:
            self._add_to_log()

        while not (self._convergent or self._divergent):
            try:
                self._check_divergence()
            except Exception as e:
                self._divergent = True
                raise e

            self._iterate_x()
            self._f_curr = self._fxn(self._x_curr)

            self._continue_loop(record_log)
        else:
            self._solution = self._x_curr
            return self._x_curr

    def _check_divergence(self) -> None:
        super()._check_divergence()
        if self._iterations > self._max_iter:
            raise ExceededMaxIterException(
                "Method exceeded maximum iterations!",
                self._max_iter
            )

    @abstractmethod
    def _iterate_x(self) -> None:
        pass

    @property
    def x0(self) -> float:
        return self._x0

    @property
    def max_iter(self) -> int:
        return self._max_iter


class Newton(Iterating):

    _cols = ["i", "x", "f_x", "f'_x", "abs_err"]

    def __init__(self,
                 fxn: MathFunction,
                 deriv: MathFunction,
                 x0: float,
                 epsilon: float = float_info.epsilon,
                 delta: float = float_info.epsilon,
                 max_iter: int = 1000
                 ):
        super().__init__(fxn, x0, epsilon, max_iter)
        self._deriv = deriv
        self._deriv_curr = deriv(x0)
        self._delta = delta

    def _iterate_x(self) -> None:
        self._x_prev = self._x_curr
        self._f_prev = self._f_curr
        deriv_prev = self._deriv_curr

        self._x_curr = self._x_prev - self._f_prev / deriv_prev
        self._deriv_curr = self._deriv(self._x_curr)

    def _check_divergence(self) -> None:
        super()._check_divergence()

        if abs(self._deriv_curr) < self._delta:
            raise ZeroSlopeException(
                "The slope of the tangent line is nearly equal to delta!",
                self._delta, self._x_curr
            )

    def _create_log_entry(self) -> Series:
        return Series({
            "i": self._iterations,
            "x": self._x_curr,
            "f_x": self._f_curr,
            "f'_x": self._deriv_curr,
            "abs_err": self._abs_err
        })

    @property
    def deriv(self) -> MathFunction:
        return self._deriv

    @property
    def delta(self) -> float:
        return self._delta


class Secant(Iterating):

    cols = ["i", "x", "f_x", "x_prev", "f_x_prev", "abs_err"]

    def __init__(self,
                 fxn: MathFunction,
                 xn1: float,
                 x0: float,
                 epsilon: float = float_info.epsilon,
                 delta: float = float_info.epsilon,
                 max_iter: int = 1000
                 ):
        super().__init__(fxn, x0, epsilon, max_iter)
        self._xn1 = xn1
        self._x_prev = xn1
        self._f_prev = fxn(xn1)
        self._delta = delta

    def _iterate_x(self) -> None:
        x_prev2, self._x_prev = self._x_prev, self._x_curr
        f_prev2, self._f_prev = self._f_prev, self._f_curr

        self._x_curr = (x_prev2 * self._f_prev -
                        self._x_prev * f_prev2) / \
            (self._f_prev - f_prev2)

    def _check_divergence(self) -> None:
        super()._check_divergence()

        if not self.is_finite_real(self._f_prev):
            raise DiscontinuityException(
                "The function is discontinuous at one point!",
                self._x_prev
            )
        elif abs(self._f_curr - self._f_prev) < self._delta:
            raise ZeroSlopeException(
                "The slope of the secant line is nearly equal to delta!",
                self._delta, self._x_curr, self._x_prev
            )

    def _create_log_entry(self) -> Series:
        return Series({
            "i": self._iterations,
            "x": self._x_curr,
            "x_prev": self._x_prev,
            "f_x": self._f_curr,
            "f_x_prev": self._f_prev,
            "abs_err": self._abs_err
        })

    @property
    def xn1(self) -> float:
        return self._xn1

    @property
    def delta(self) -> float:
        return self._delta


class SuccSubs(Iterating):

    _cols = ["i", "x", "f_x", "g_x", "abs_err"]

    def __init__(self,
                 fxn: MathFunction,
                 aux: MathFunction,
                 x0: float,
                 epsilon: float = float_info.epsilon,
                 max_iter: int = 1000):
        super().__init__(fxn, x0, epsilon, max_iter)
        self._aux = aux
        self._aux_curr = aux(x0)

    def _iterate_x(self) -> None:
        self._x_prev = self._x_curr
        self._f_prev = self._f_curr
        self._x_curr = self._aux_curr
        self._aux_curr = self._aux(self._x_curr)

    def _check_divergence(self) -> None:
        super()._check_divergence()

        if not super().is_finite_real(self._aux_curr):
            raise DiscontinuityException(
                "The auxiliary function is discontinuous at a point!",
                self._x_curr
            )

    def _create_log_entry(self) -> Series:
        return Series({
            "i": self._iterations,
            "x": self._x_curr,
            "f_x": self._f_curr,
            "g_x": self._aux_curr,
            "abs_err": self._abs_err
        })

    @property
    def gxn(self) -> MathFunction:
        return self._aux
