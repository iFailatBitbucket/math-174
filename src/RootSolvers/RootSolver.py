from abc import ABC, abstractmethod
from sys import float_info
from typing import Callable

from numpy import isfinite, isreal, Inf
from pandas import DataFrame, Series

from ..CustomTypes import MathFunction
from ..Exceptions import DiscontinuityException


class RootSolver(ABC):

    _iterations = 0
    _solution = None
    _convergent = False
    _divergent = False
    _x_curr = None
    _x_prev = None
    _f_curr = None
    _abs_err = None
    _log = None
    _cols = ["i", "x", "f_x", "abs_err"]

    def __init__(self,
                 fxn: MathFunction,
                 epsilon: float = float_info.epsilon):
        self._fxn = fxn
        self._epsilon = epsilon
        self._log = DataFrame(columns=self._cols)

    @abstractmethod
    def solve_root(self, record_log: bool = False) -> float:
        pass

    @staticmethod
    def is_finite_real(*args) -> bool:
        for arg in args:
            if not isfinite(arg) or not isfinite(arg):
                return False

        return True

    def _check_divergence(self) -> None:
        if not self.is_finite_real(self._x_curr):
            raise DiscontinuityException(
                "The function is discontinuous on a point!",
                self._x_curr
            )

    def _create_log_entry(self) -> Series:
        return Series({
            "i": self._iterations,
            "x": self._x_curr,
            "f_x": self._f_curr,
            "abs_err": self._abs_err
        })

    def _add_to_log(self) -> None:
        new_entry = self._create_log_entry()
        self._log = self._log.append(new_entry, ignore_index=True)

    def _continue_loop(self, record_log: bool = False) -> None:
        self._iterations += 1
        self._abs_err = Inf if self._x_prev is None else \
            abs(self._x_curr - self._x_prev)
        self._convergent = self._abs_err < self._epsilon
        if record_log:
            self._add_to_log()

    @property
    def fxn(self) -> MathFunction:
        return self._fxn

    @property
    def epsilon(self) -> float:
        return self._epsilon

    @property
    def iterations(self) -> int:
        return self._iterations

    @property
    def solution(self) -> float:
        return self._solution

    @property
    def divergent(self) -> bool:
        return self._divergent

    @property
    def convergent(self) -> bool:
        return self._convergent

    @property
    def abs_err(self) -> float:
        return self._abs_err

    @property
    def log(self) -> DataFrame:
        return self._log
