from abc import abstractmethod
from sys import float_info
from typing import Callable

from numpy import Inf
from pandas import Series

from ..CustomTypes import MathFunction
from ..Exceptions import DiscontinuityException, IVTViolationException
from .RootSolver import RootSolver


class Bracketing(RootSolver):

    _cols = ["i", "x", "a", "b", "f_x", "f_a", "f_b", "abs_err"]

    def __init__(self,
                 fxn: MathFunction,
                 left: float,
                 right: float,
                 epsilon: float = float_info.epsilon
                 ):
        super().__init__(fxn, epsilon)

        self._left = left
        self._curr_left = left
        self._f_left = fxn(left)

        self._right = right
        self._curr_right = right
        self._f_right = fxn(right)

        self._check_endpoints()

    def _check_endpoints(self) -> None:
        if not self.is_finite_real(self._f_left, self._f_right):
            raise DiscontinuityException(
                "The function is discontinuous at the endpoints!",
                self._f_left, self._f_right
            )
        elif self._f_left * self._f_right > 0:
            raise IVTViolationException(
                "Endpoints must satisfy the intermediate value theorem!",
                self._left, self._right
            )

    def solve_root(self, record_log: bool = False) -> float:
        while not (self._convergent or self._divergent):
            self._x_prev = self._x_curr
            self._calculate_x_curr()
            self._f_curr = self._fxn(self._x_curr)

            try:
                self._check_divergence()
            except Exception as e:
                self._divergent = True
                raise e

            self._continue_loop(record_log)
            self._tighten_brackets()
        else:
            self._solution = self._x_curr
            return self._x_curr

    @abstractmethod
    def _calculate_x_curr(self) -> None:
        pass

    @abstractmethod
    def _tighten_brackets(self) -> None:
        pass

    def _create_log_entry(self) -> Series:
        return Series({
            "i": self._iterations,
            "x": self._x_curr,
            "f_x": self._f_curr,
            "a": self._curr_left,
            "f_a": self._f_left,
            "b": self._curr_right,
            "f_b": self._f_right,
            "abs_err": self._abs_err
        })

    @property
    def left(self) -> float:
        return self._left

    @property
    def right(self) -> float:
        return self._right


class Bisection(Bracketing):
    def __init__(self,
                 fxn: MathFunction,
                 left: float,
                 right: float,
                 epsilon: float = float_info.epsilon
                 ):
        super().__init__(fxn, left, right, epsilon)

    def _calculate_x_curr(self) -> None:
        self._x_curr = (self._curr_left + self._curr_right) / 2

    def _tighten_brackets(self) -> None:
        if self._f_curr * self._f_left > 0:
            self._curr_left = self._x_curr
            self._f_left = self._f_curr

        elif self._f_curr * self._f_right > 0:
            self._curr_right = self._x_curr
            self._f_right = self._f_curr

        else:
            self._convergent = True


class RegulaFalsi(Bracketing):
    def __init__(self,
                 fxn: MathFunction,
                 left: float,
                 right: float,
                 epsilon: float = float_info.epsilon
                 ):
        super().__init__(fxn, left, right, epsilon)

    def _calculate_x_curr(self) -> None:
        self._x_curr = (self._curr_left * self._f_right -
                        self._curr_right * self._f_left) / \
            (self._f_right - self._f_left)

    def _tighten_brackets(self) -> None:
        if self._f_curr * self._f_left > 0:
            self._curr_left = self._x_curr
            self._f_left = self._f_curr

        elif self._f_curr * self._f_right > 0:
            self._curr_right = self._x_curr
            self._f_right = self._f_curr

        else:
            self._convergent = True
