from .Bracketing import Bisection, RegulaFalsi
from .Iterating import Newton, Secant, SuccSubs
