import numpy as np

from ..Exceptions import InvalidPtListException
from .PolyInterps import PolyInterp


class NewtonDiff(PolyInterp):

    _diff_table = None

    def __init__(self, point_list: np.ndarray):
        super().__init__(point_list)
        self._step = np.abs(self._x_vals[1] - self._x_vals[0])

    @staticmethod
    def _check_point_list(point_list: np.ndarray):
        super()._check_point_list(point_list)

        x_vals = point_list[:, 0]
        sort_indexes = np.argsort(x_vals)
        point_list = point_list[sort_indexes, :]

        step = x_vals[1] - x_vals[0]
        pt_len = len(point_list[:, 0])

        for i in range(2, pt_len):
            if step != x_vals[i] - x_vals[i - 1]:
                raise InvalidPtListException(
                    "Step length between two x-values must be uniform!",
                    point_list
                )

    @property
    def diff_table(self) -> np.ndarray:
        return self._diff_table

    @property
    def step(self) -> float:
        return self._step


class FwdDiff(NewtonDiff):
    def _calc_coeffs(self) -> np.ndarray:
        self._diff_table = np.full((self._len, self._len), np.nan)
        self._diff_table[:, 0] = self._f_vals

        for order in range(1, self._len):
            for index in range(self._len - order):
                diff = self._diff_table[index + 1, order - 1] - \
                    self._diff_table[index, order - 1]
                self._diff_table[index, order] = diff

        return self._diff_table[0]

    def _calc_f(self, x: float) -> float:
        ret_val = 0
        s_val = (x - self._x_vals[0]) / self._step
        s_binom = 1

        for i, coeff in enumerate(self._coeffs):
            s_binom *= (s_val - i + 1) / i if i != 0 else 1
            term = coeff * s_binom

            ret_val += term

        return ret_val


class BackDiff(NewtonDiff):
    def _calc_coeffs(self) -> np.ndarray:
        self._diff_table = np.full((self._len, self._len), np.nan)
        self._diff_table[:, 0] = self._f_vals

        for order in range(1, self._len):
            for index in range(order, self._len):
                diff = self._diff_table[index, order - 1] - \
                    self._diff_table[index - 1, order - 1]
                self._diff_table[index, order] = diff

        return self._diff_table[-1]

    def _calc_f(self, x: float) -> float:
        ret_val = 0
        i_factorial = 1
        s_val = (x - self._x_vals[-1]) / self._step
        factor = 1

        for i, coeff in enumerate(self._coeffs):
            factor *= (s_val + i - 1) / i if i != 0 else 1
            term = (coeff * factor) / i_factorial
            ret_val += term

        return ret_val
