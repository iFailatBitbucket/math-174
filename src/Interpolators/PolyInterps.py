from abc import ABC, abstractmethod

import numpy as np

from ..CustomTypes import FloatGen
from ..Exceptions import DuplicateXException, InvalidPtListException


class PolyInterp(ABC):

    _coeffs = None

    def __init__(self, point_list: np.ndarray):
        sort_indexes = np.argsort(point_list[:, 0])
        point_list = point_list[sort_indexes, :]

        self._x_vals = point_list[:, 0]
        self._f_vals = point_list[:, 1]

        self._len = len(self._x_vals)
        self._coeffs = self._calc_coeffs()

    @staticmethod
    def _check_point_list(point_list: np.ndarray):
        is_finite_real = np.logical_and(np.isreal(point_list),
                                        np.isfinite(point_list))
        dims = point_list.shape
        err_msg = ""

        if len(dims) != 2:
            err_msg = "Point list must have 2 dimensions!"
        elif dims[0] < 2:
            err_msg = "Point list must have at least 2 points!"
        elif dims[1] != 2:
            err_msg = "Point list must have 2 values per point!"
        elif not np.all(is_finite_real):
            err_msg = "Point values must be real and finite!"

        if len(err_msg) != 0:
            raise InvalidPtListException(err_msg, point_list)

        x_vals = point_list[:, 0]
        if len(x_vals) != len(np.unique(x_vals)):
            raise DuplicateXException("x values must be unique!", *x_vals)

    def interpolate(self, x: float) -> float:
        equal_to_x = np.where(self._x_vals == x)
        if np.any(equal_to_x):
            return self._f_vals[equal_to_x]

        return self._calc_f(x)

    @abstractmethod
    def _calc_coeffs(self) -> np.ndarray:
        pass

    @abstractmethod
    def _calc_f(self, x: float) -> float:
        pass

    @property
    def x_vals(self) -> np.ndarray:
        return self._x_vals

    @property
    def f_vals(self) -> np.ndarray:
        return self._f_vals

    @property
    def coeffs(self) -> np.ndarray:
        return self._coeffs

    def __len__(self) -> int:
        return self._len


class Lagrange(PolyInterp):
    def _gen_factors_except(self, x: float,
                            skip_index: int = None) -> FloatGen:
        for i in range(self._len):
            if skip_index is None or i != skip_index:
                yield x - self._x_vals[i]
            else:
                continue

    def _calc_coeffs(self) -> np.ndarray:
        coeffs = np.ones(self._len)

        for i, x_i in enumerate(self._x_vals):
            for factor in self._gen_factors_except(x_i, i):
                coeffs[i] *= factor

        return coeffs

    def _calc_f(self, x: float) -> float:
        prod_poly = 1
        sum_bases = 0

        for f_val, coeff, factor in zip(self._f_vals, self._coeffs,
                                        self._gen_factors_except(x)):
            prod_poly *= factor
            sum_bases += f_val / (factor * coeff)

        return prod_poly * sum_bases


class UndetCoeffs(PolyInterp):
    def _gen_pows(self, x: float) -> FloatGen:
        for i in range(self._len):
            yield x ** i

    def _calc_coeffs(self) -> np.ndarray:
        lhs = np.full((self._len, self._len), np.nan)

        for i, x_i in enumerate(self._x_vals):
            for j, x_raised in enumerate(self._gen_pows(x_i)):
                lhs[i, j] = x_raised

        return np.linalg.solve(lhs, self._f_vals)

    def _calc_f(self, x: float) -> float:
        ret_val = 0

        for coeff, x_raised in zip(self._coeffs, self._gen_pows(x)):
            ret_val += coeff * x_raised

        return ret_val


class DivDiff(PolyInterp):

    _diff_table = None

    def _calc_coeffs(self) -> np.ndarray:
        self._diff_table = np.full((self._len, self._len), np.nan)
        self._diff_table[:, 0] = self._f_vals

        for order in range(1, self._len):
            for index in range(self._len - order):
                diff = self._diff_table[index + 1, order - 1] - \
                    self._diff_table[index, order - 1]

                delta = self._x_vals[index + order] - self._x_vals[index]
                self._diff_table[index, order] = diff / delta

        return self._diff_table[0]

    def _calc_f(self, x: float) -> float:
        ret_val = 0
        prod_of_factors = 1

        for i, coeff in enumerate(self._coeffs):
            prod_of_factors *= (x - self._x_vals[i - 1]) if i != 0 else 1
            term = coeff * prod_of_factors
            ret_val += term

        return ret_val

    @property
    def diff_table(self) -> np.ndarray:
        return self._diff_table
