from .NewtonDiff import BackDiff, FwdDiff
from .PolyInterps import DivDiff, Lagrange, UndetCoeffs
