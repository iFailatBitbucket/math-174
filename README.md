# Math 174 - Numerical Methods

Just an implementation of methods discussed so far from Math 174, numerical
approximations.

## Prerequisites
The user must have Python 3 (preferably 3.7.0) installed, and the following
packages from pip.

- numpy
- sympy