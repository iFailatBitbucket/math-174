import numpy as np

from src.Interpolators import *
from src.RootSolvers import *

if __name__ == "__main__":
    def fxn(x: float) -> float: return x - np.cos(x)

    def aux(x: float) -> float: return np.cos(x)

    def deriv(x: float) -> float: return 1 + np.sin(x)
    left = 0
    right = 1
    x0 = 0
    xn1 = 1

    bisection = Bisection(fxn, left, right)
    regfalsi = RegulaFalsi(fxn, left, right)
    newton = Newton(fxn, deriv, x0)
    secant = Secant(fxn, xn1, x0)
    succsub = SuccSubs(fxn, aux, x0)

    bisection.solve_root()
    regfalsi.solve_root()
    newton.solve_root()
    secant.solve_root()
    succsub.solve_root()

    print("Bisection Solution:", bisection.solution,
          "Iterations:", bisection.iterations)

    print("Regula Falsi Solution:", regfalsi.solution,
          "Iterations:", regfalsi.iterations)

    print("Newton Solution:", newton.solution,
          "Iterations:", newton.iterations)

    print("Secant Solution:", secant.solution,
          "Iterations:", secant.iterations)

    print("Successive Substitution Solution:", succsub.solution,
          "Iterations:", succsub.iterations)

    points = np.array([[-2., 1.],
                       [-1., -2.],
                       [0., 1.],
                       [1., 4.],
                       [2., 25.]])

    x = 3.

    lagrange = Lagrange(points)
    undetcoeffs = UndetCoeffs(points)
    divdiff = DivDiff(points)
    fwddiff = FwdDiff(points)
    backdiff = BackDiff(points)

    print("Lagrange:", lagrange.interpolate(x))
    print("Undetermined Coefficients:", undetcoeffs.interpolate(x))
    print("Divided Difference:", divdiff.interpolate(x))
    print("Divided Difference Table:", divdiff.diff_table, sep='\n')
    print("Forward Difference:", fwddiff.interpolate(x))
    print("Forward Difference Table:", fwddiff.diff_table, sep='\n')
    print("Backward Difference:", backdiff.interpolate(x))
    print("Backward Difference Table:", backdiff.diff_table, sep='\n')
